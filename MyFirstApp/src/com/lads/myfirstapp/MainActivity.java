package com.lads.myfirstapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends Activity {
	
	public String previousState=""; 

    public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
    public static String passed_message;

    @Override
    protected void onCreate(Bundle savedInstanceState) { 
        super.onCreate(savedInstanceState);
        System.out.println("Created");
        
        setContentView(R.layout.activity_main); //Goes to main activity
   
    }
    
    @Override
    protected void onStart() {
        super.onStart();  // Always call the superclass method first
        
        String state = "started";
        outputActivityState(state);
        
        previousState = "\"started\"";
        
       
       // The activity is either being restarted or started for the first time

    }
    

	@Override
    protected void onRestart() {
        super.onRestart();  // Always call the superclass method first
        System.out.println("Restarted");
        
        String state = "restarted";
        outputActivityState(state);
        
        previousState = "\"restarted\"";

        // Activity being restarted from stopped state    
    }
    
    @Override
    protected void onStop() {
        super.onStop();  // Always call the superclass method first

    }
    
    @Override
    public void onPause() {
        super.onPause(); // Always call the superclass method first
        
        String state = "paused";
        outputActivityState(state);
        
        Log.d("MainActivity", "paused!!!");
        
        previousState = "\"paused\"";
        

    }

    public void onResume() {
        super.onResume();  // Always call the superclass method first
        
      
        String state = "resumed";
        outputActivityState(state);
        
        previousState = "\"resumed\"";
        
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    
    public void openSearch() { System.out.println("Hello, World");} //Prints "Hello World" to console when magnifying class selected on action bar
    
    
    public void sendMessage (View view){ 
    	
    	Intent intent = new Intent(this, DisplayMessageActivity.class);
    	EditText editText = (EditText) findViewById(R.id.edit_message);
    	String message = editText.getText().toString();
    	intent.putExtra(EXTRA_MESSAGE, message);
    	passed_message = message;
    	startActivity(intent);
    	
    }
    
  
    public void openSettings() { ; } //Function for settings button on action bar
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) { //action bar
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_settings:
                openSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    
    private void outputActivityState(String state) { //Outputs the state of the main activity in main activity
    	state = "I've been \"" + state;
    	state = state + "\" after being " + previousState;
        TextView activity_state = (TextView)findViewById(R.id.activity_state);
        activity_state.setTextSize(20);
        activity_state.setText(state);
        activity_state.setX(10);
        activity_state.setY(100);
    }

    
    
}
